let g:tex_flavor='latex'
set tabstop=2
set softtabstop=4
set expandtab
set shiftwidth=2
set textwidth=120
set colorcolumn=121
