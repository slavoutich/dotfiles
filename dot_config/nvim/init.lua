-- set mapleader before all other configuration
vim.g.mapleader = ' '
vim.cmd [[
set autoindent
set expandtab
set shiftwidth=2
set smartindent
set softtabstop=2
set tabstop=2
]]

local not_vscode = vim.g.vscode == nil

-- load plugins
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    {
        'nvim-lualine/lualine.nvim',
        config = function() require('plugins.lualine') end,
        cond = not_vscode,
    },
    { "RRethy/base16-nvim", cond = not_vscode },
    {
        'ms-jpq/chadtree',
        branch = 'chad',
        build = 'python3 -m chadtree deps',
        cond = not_vscode,
    },
    -- Language server etc.
    -- TODO: consider migration to https://github.com/ms-jpq/coq_nvim
    {
        'neoclide/coc.nvim',
        branch = 'release',
        config = function()
            vim.g.coc_global_extensions = {
                'coc-go',
                'coc-json',
                'coc-yaml',
                'coc-pyright',
                'coc-rust-analyzer',
                'coc-snippets',
                'coc-sumneko-lua',
                'coc-tsserver',
                '@yaegassy/coc-ansible',
            }
            vim.cmd "let g:coc_filetype_map = {'yaml.ansible': 'ansible'}"
        end,
        cond = not_vscode,
    },
    -- Debugger
    {
        "rcarriga/nvim-dap",
        config = function() require('plugins.dap') end,
        cond = not_vscode,
    },
    {
        "rcarriga/nvim-dap-ui",
        dependencies = { "mfussenegger/nvim-dap" },
        config = function() require('plugins.dapui') end,
        cond = not_vscode,
    },
    -- Python debugger
    {
        "mfussenegger/nvim-dap-python",
        dependencies = { "mfussenegger/nvim-dap" },
        config = function() require('plugins.dap_python') end,
        cond = not_vscode,
    },
    -- Telescope
    {
        'nvim-telescope/telescope.nvim',
        branch = '0.1.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-treesitter/nvim-treesitter',
            {
                "nvim-telescope/telescope-fzf-native.nvim",
                build = "make",
                enabled = vim.fn.executable("make") == 1,
            },
            'fannheyward/telescope-coc.nvim',
            'cljoly/telescope-repo.nvim',
            {
                'nvim-telescope/telescope-dap.nvim',
                dependencies = { "mfussenegger/nvim-dap" },
            },
        },
        config = function() require('plugins.telescope') end,
        cond = not_vscode,
    },
    -- Jumping across text
    {
        'phaazon/hop.nvim',
        branch = 'v2', -- optional but strongly recommended
        config = function() require('plugins.hop') end,
    },
    -- comments
    {
        'numToStr/Comment.nvim',
        config = function() require('plugins.comment') end,
    },
    -- Language definitions
    {
        "sheerun/vim-polyglot",
        cond = not_vscode,
    },
    {
        "mfussenegger/nvim-ansible",
        cond = not_vscode,
    },
    -- testing
    {
        "nvim-neotest/neotest",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-treesitter/nvim-treesitter",
            "antoinemadec/FixCursorHold.nvim",
            "nvim-neotest/nvim-nio",
            -- adapters
            "nvim-neotest/neotest-python",
        },
        config = function() require('plugins.neotest') end,
        cond = not_vscode,
    },
    -- git
    {
        "NeogitOrg/neogit",
        dependencies = {
            'nvim-lua/plenary.nvim',
            'sindrets/diffview.nvim',
        },
        config = function() require('plugins.neogit') end,
        cond = not_vscode,
    },
    {
        "f-person/git-blame.nvim",
        cond = not_vscode,
    },
    -- TODO: integrate git-blame with statusline
    -- Surround text with brackets
    {
        "kylechui/nvim-surround",
        config = function() require('nvim-surround').setup {} end,
    },
    -- Pairs of brackets
    {
        "windwp/nvim-autopairs",
        config = function() require("nvim-autopairs").setup {} end,
        cond = not_vscode,
    },
    {
        'nvim-tree/nvim-web-devicons',
        config = function()
            require('nvim-web-devicons').setup {
                color_icons = true,
                default = true,
            }
        end,
        cond = not_vscode,
    },
    {
        "iamcco/markdown-preview.nvim",
        run = "cd app && npm install",
        setup = function() vim.g.mkdp_filetypes = { "markdown" } end,
        ft = { "markdown" },
        cond = not_vscode,
    },
    {
        "elkowar/yuck.vim",
        cond = not_vscode,
    },
    {
        "xiyaowong/transparent.nvim",
        cond = not_vscode,
    },
    {
        '4e554c4c/darkman.nvim',
        build = 'go build -o bin/darkman.nvim',
        config = function()
            require('darkman').setup {
                colorscheme = { dark = "base16-gruvbox-dark-hard", light = "base16-gruvbox-light-hard" }
            }
        end,
        cond = not_vscode,
    },
    {

        "amitds1997/remote-nvim.nvim",
        version = "*",                       -- Pin to GitHub releases
        dependencies = {
            "nvim-lua/plenary.nvim",         -- For standard functions
            "MunifTanjim/nui.nvim",          -- To build the plugin UI
            "nvim-telescope/telescope.nvim", -- For picking b/w different remote methods
        },
        config = true,
        cond = not_vscode,
    },
})

vim.opt.shiftwidth = 4

-- Some servers have issues with backup files, see #649.
-- vim.opt.backup = false
-- vim.opt.writebackup = false

-- Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
-- delays and poor user experience.
vim.opt.updatetime = 300

-- Always show the signcolumn, otherwise it would shift the text each time
-- diagnostics appear/become resolved.
vim.opt.signcolumn = "yes"

vim.api.nvim_create_autocmd(
    "BufWritePre", { command = "%s/\\s\\+$//e" }
)

if vim.fn.has("unix") then
    vim.g.python3_host_prog = "/usr/bin/python3"
elseif vim.fn.has("win32") then
    vim.g.python3_host_prog = "C:\\Users\\micro\\mambaforge\\python.exe"
end

-- set keymaps
require('keymaps')

-- configure gui
vim.opt.guifont = "FiraCode Nerd Font:h11"
vim.g.neovide_transparency = 0.95
vim.g.neovide_normal_opacity = 0.95
