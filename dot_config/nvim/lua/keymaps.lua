local bind = vim.keymap.set
local remap = function(m, lhs, rhs) bind(m, lhs, rhs, { remap = true }) end
local noremap = function(m, lhs, rhs) bind(m, lhs, rhs, { remap = false }) end

-- misc
noremap('n', '<C-w>i', ':vsplit<CR>')
noremap('n', '<C-w>o', ':split<CR>')

-- Ranger.nvim maps
-- c.remap('n', '<leader>a', ':Ranger .<CR>')
-- c.remap('n', '<leader>A', ':Ranger ~<CR>')
-- Plugins active in both VSCode and vanilla nvim

-- hop.nvim maps
local hop = require('hop')
remap('n', ';', hop.hint_char2)

if vim.g.vscode == nil then
    -- Keymaps when VSCode is not active
    --
    -- CHADtree maps
    bind('n', '<leader>d', ':CHADopen<CR>', { remap = false, silent = true })
    bind('n', '<leader>D', ':CHADopen ~<CR>', { remap = false, silent = true })

    -- Neogit
    bind('n', '<leader>g', ':Neogit<CR>', { remap = false, silent = true })

    -- Telescope.nvim maps
    local telescope = require('telescope.builtin')
    remap('n', ',.', telescope.buffers)
    remap('n', ',g', telescope.live_grep)
    remap('n', ',h', telescope.help_tags)
    remap('n', ',,', telescope.find_files)
    remap('n', ',w', function() vim.cmd 'Telescope coc workspace_symbols' end)
    remap('n', ',s', function() vim.cmd 'Telescope coc document_symbols' end)
    remap('n', ',r', function() vim.cmd 'Telescope coc references' end)
    -- c.remap('n', ',d', function() vim.cmd 'Telescope coc definitions' end)
    -- c.remap('n', ',D', function() vim.cmd 'Telescope coc declarations' end)
    remap('n', ',a', function() vim.cmd 'Telescope coc file_code_actions' end)
    remap('n', ',l', function() vim.cmd 'Telescope coc line_code_actions' end)
    remap('n', ',r', function() vim.cmd 'Telescope repo list' end)
    remap('n', ',dd', function() vim.cmd 'Telescope dap variables' end)
    remap('n', ',db', function() vim.cmd 'Telescope dap list_breakpoints' end)

    -- coc.nvim
    -- Auto complete
    function _G.check_back_space()
        local col = vim.fn.col('.') - 1
        return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
    end

    -- Use tab for trigger completion with characters ahead and navigate.
    -- NOTE: There's always complete item selected by default, you may want to enable
    -- no select by `"suggest.noselect": true` in your configuration file.
    -- NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
    -- other plugin before putting this into your config.
    local opts = { silent = true, noremap = true, expr = true, replace_keycodes = false }
    bind("i", "<TAB>", 'coc#pum#visible() ? coc#pum#next(1) : v:lua.check_back_space() ? "<TAB>" : coc#refresh()', opts)
    bind("i", "<S-TAB>", [[coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"]], opts)

    -- Make <CR> to accept selected completion item or notify coc.nvim to format
    -- <C-g>u breaks current undo, please make your own choice.
    bind("i", "<cr>", [[coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]], opts)

    -- Use <c-j> to trigger snippets
    bind("i", "<c-j>", "<Plug>(coc-snippets-expand-jump)")
    -- Use <c-space> to trigger completion.
    bind("i", "<c-space>", "coc#refresh()", { silent = true, expr = true })

    -- Use `[g` and `]g` to navigate diagnostics
    -- Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
    bind("n", "[g", "<Plug>(coc-diagnostic-prev)", { silent = true })
    bind("n", "]g", "<Plug>(coc-diagnostic-next)", { silent = true })

    -- GoTo code navigation.
    bind("n", "gd", "<Plug>(coc-definition)", { silent = true })
    bind("n", "gy", "<Plug>(coc-type-definition)", { silent = true })
    bind("n", "gi", "<Plug>(coc-implementation)", { silent = true })
    bind("n", "gr", "<Plug>(coc-references)", { silent = true })


    -- Use K to show documentation in preview window.
    function _G.show_docs()
        local cw = vim.fn.expand('<cword>')
        if vim.fn.index({ 'vim', 'help' }, vim.bo.filetype) >= 0 then
            vim.api.nvim_command('h ' .. cw)
        elseif vim.api.nvim_eval('coc#rpc#ready()') then
            vim.fn.CocActionAsync('doHover')
        else
            vim.api.nvim_command('!' .. vim.o.keywordprg .. ' ' .. cw)
        end
    end

    bind("n", "K", '<CMD>lua _G.show_docs()<CR>', { silent = true })


    -- Highlight the symbol and its references when holding the cursor.
    vim.api.nvim_create_augroup("CocGroup", {})
    vim.api.nvim_create_autocmd("CursorHold", {
        group = "CocGroup",
        command = "silent call CocActionAsync('highlight')",
        desc = "Highlight symbol under cursor on CursorHold"
    })


    -- Symbol renaming.
    bind("n", "<leader>rn", "<Plug>(coc-rename)", { silent = true })


    -- Formatting selected code.
    bind("n", "<leader>f", function() vim.fn.call("CocAction", { "format" }) end, { remap = false })
    bind("x", "<leader>f", "<Plug>(coc-format-selected)", { silent = true })


    -- Setup formatexpr specified filetype(s).
    vim.api.nvim_create_autocmd("FileType", {
        group = "CocGroup",
        pattern = "typescript,json",
        command = "setl formatexpr=CocAction('formatSelected')",
        desc = "Setup formatexpr specified filetype(s)."
    })

    -- Update signature help on jump placeholder.
    vim.api.nvim_create_autocmd("User", {
        group = "CocGroup",
        pattern = "CocJumpPlaceholder",
        command = "call CocActionAsync('showSignatureHelp')",
        desc = "Update signature help on jump placeholder"
    })


    -- Applying codeAction to the selected region.
    -- Example: `<leader>aap` for current paragraph
    -- local opts = {silent = true, nowait = true}
    -- c.bind("x", "<leader>a", "<Plug>(coc-codeaction-selected)", opts)
    -- c.bind("n", "<leader>a", "<Plug>(coc-codeaction-selected)", opts)

    -- Remap keys for applying codeAction to the current buffer.
    -- c.bind("n", "<leader>ac", "<Plug>(coc-codeaction)", opts)


    -- Apply AutoFix to problem on the current line.
    bind("n", "<leader>qf", "<Plug>(coc-fix-current)", opts)


    -- Run the Code Lens action on the current line.
    bind("n", "<leader>cl", "<Plug>(coc-codelens-action)", opts)


    -- Map function and class text objects
    -- NOTE: Requires 'textDocument.documentSymbol' support from the language server.
    bind("x", "if", "<Plug>(coc-funcobj-i)", opts)
    bind("o", "if", "<Plug>(coc-funcobj-i)", opts)
    bind("x", "af", "<Plug>(coc-funcobj-a)", opts)
    bind("o", "af", "<Plug>(coc-funcobj-a)", opts)
    bind("x", "ic", "<Plug>(coc-classobj-i)", opts)
    bind("o", "ic", "<Plug>(coc-classobj-i)", opts)
    bind("x", "ac", "<Plug>(coc-classobj-a)", opts)
    bind("o", "ac", "<Plug>(coc-classobj-a)", opts)


    -- Remap <C-f> and <C-b> for scroll float windows/popups.
    ---@diagnostic disable-next-line: redefined-local
    local opts = { silent = true, nowait = true, expr = true }
    bind("n", "<C-f>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', opts)
    bind("n", "<C-b>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', opts)
    bind("i", "<C-f>",
        'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(1)<cr>" : "<Right>"', opts)
    bind("i", "<C-b>",
        'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(0)<cr>" : "<Left>"', opts)
    bind("v", "<C-f>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', opts)
    bind("v", "<C-b>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', opts)


    -- Use CTRL-S for selections ranges.
    -- Requires 'textDocument/selectionRange' support of language server.
    bind("n", "<C-s>", "<Plug>(coc-range-select)", { silent = true })
    bind("x", "<C-s>", "<Plug>(coc-range-select)", { silent = true })


    -- Add `:Format` command to format current buffer.

    -- " Add `:Fold` command to fold current buffer.
    vim.api.nvim_create_user_command("Fold", "call CocAction('fold', <f-args>)", { nargs = '?' })

    -- Add `:OR` command for organize imports of the current buffer.
    vim.api.nvim_create_user_command("OR", "call CocActionAsync('runCommand', 'editor.action.organizeImport')", {})

    -- Add (Neo)Vim's native statusline support.
    -- NOTE: Please see `:h coc-status` for integrations with external plugins that
    -- provide custom statusline: lightline.vim, vim-airline.
    vim.opt.statusline:prepend("%{coc#status()}%{get(b:,'coc_current_function','')}")

    -- Mappings for CoCList
    -- code actions and coc stuff
    ---@diagnostic disable-next-line: redefined-local
    -- local opts = {silent = true, nowait = true}
    -- Show all diagnostics.
    -- c.bind("n", "<space>a", ":<C-u>CocList diagnostics<cr>", opts)
    -- Go to the next diagnostics issue
    bind("n", "gt", "<Plug>(coc-diagnostics-next)")
    bind("n", "gT", "<Plug>(coc-diagnostics-prev)")
    -- Manage extensions.
    -- c.bind("n", "<space>e", ":<C-u>CocList extensions<cr>", opts)
    -- Show commands.
    -- c.bind("n", "<space>c", ":<C-u>CocList commands<cr>", opts)
    -- Find symbol of current document.
    -- c.bind("n", "<space>o", ":<C-u>CocList outline<cr>", opts)
    -- Search workspace symbols.
    -- c.bind("n", "<space>s", ":<C-u>CocList -I symbols<cr>", opts)
    -- Do default action for next item.
    -- c.bind("n", "<space>j", ":<C-u>CocNext<cr>", opts)
    -- Do default action for previous item.
    -- c.bind("n", "<space>k", ":<C-u>CocPrev<cr>", opts)
    -- Resume latest coc list.
    -- c.bind("n", "<space>p", ":<C-u>CocListResume<cr>", opts)
else
    -- VSCode-only keybindings
    local vscode = require("vscode-neovim")
    bind('n', '<leader>d', function() vscode.action("workbench.explorer.fileView.focus") end,
        { remap = false, silent = true })
    bind('n', '<leader> ', function() vscode.action("workbench.action.toggleSidebarVisibility") end)
    bind('n', ',,', function() vscode.action("workbench.action.quickOpen") end)
    bind('n', ',g', function() vscode.action("workbench.view.scm") end)
    bind('n', ',w', function() vscode.action("workbench.action.showAllSymbols") end)
    bind('n', ',d', function() vscode.action("workbench.action.gotoSymbol") end)
end
