require('Comment').setup {
	toggler = {
		line = '<leader>cc',
		block = '<leader>cx',
	},
	opleader = {
		line = '<leader>cc',
		block = '<leader>cx',
	},
	extra = {
		above = '<leader>cO',
		below = '<leader>co',
		eol = '<leader>cA',
	},
}
