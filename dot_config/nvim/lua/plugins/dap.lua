local dap = require('dap')
local bind = vim.keymap.set

dap.configurations.python = {
    {
        -- The first three options are required by nvim-dap
        type = 'python', -- the type here established the link to the adapter definition: `dap.adapters.python`
        request = 'launch',
        name = "Launch file",

        -- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options
        program = "${file}", -- This configuration will launch the current file if used.
        pythonPath = function()
            -- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
            -- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
            -- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
            local cwd = vim.fn.getcwd()
            if vim.fn.executable(cwd .. '/venv/bin/python') == 1 then
                return cwd .. '/venv/bin/python'
            elseif vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
                return cwd .. '/.venv/bin/python'
            else
                return '/usr/bin/python3'
            end
        end,
    },
}
dap.configurations.cpp = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        args = {},

        -- 💀
        -- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
        --
        --    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
        --
        -- Otherwise you might get the following error:
        --
        --    Error on launch: Failed to attach to the target process
        --
        -- But you should be aware of the implications:
        -- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
        -- runInTerminal = false,
    },
}

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp

bind('n', '<F5>', dap.continue, { remap = false, silent = true })
bind('n', '<c-d>b', dap.toggle_breakpoint, { remap = false, silent = true })
bind('n', '<c-d>B', function() dap.set_breakpoint(vim.fn.input('Breakpoint condition: ')) end,
    { remap = false, silent = true })
bind('n', '<c-d>l', function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end,
    { remap = false, silent = true })
bind('n', '<F10>', dap.step_over, { remap = false, silent = true })
bind('n', '<F11>', dap.step_into, { remap = false, silent = true })
bind('n', '<F12>', dap.step_out, { remap = false, silent = true })
bind('n', '<c-d>r', dap.run_last, { remap = false, silent = true })
bind('n', '<c-d><c-c>', dap.terminate, { remap = false, silent = true })
bind('n', '<c-d><c-d>', function() if dap.session() == nil then dap.run_last() else dap.restart() end end,
    { remap = false, silent = true })

-- nnoremap <silent> <Leader>dr <Cmd>lua require'dap'.repl.open()<CR>
