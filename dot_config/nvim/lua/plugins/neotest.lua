local neotest = require("neotest")
local bind = vim.keymap.set
local remap = function(m, lhs, rhs) bind(m, lhs, rhs, { remap = true }) end

neotest.setup({
    default_strategy = "integrated",
    summary = {
        animated = false,
    },
    quickfix = {
        enabled = true,
        open = false,
    },
    adapters = {
        require("neotest-python")({
            animated = false,
            -- Extra arguments for nvim-dap configuration
            -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
            dap = { justMyCode = false },
            -- Command line arguments for runner
            -- Can also be a function to return dynamic values
            args = { "--log-level", "WARN" },
            -- Runner to use. Will use pytest if available by default.
            -- Can be a function to return dynamic value.
            runner = "pytest",
            -- Custom python path for the runner.
            -- Can be a string or a list of strings.
            -- Can also be a function to return dynamic value.
            -- If not provided, the path will be inferred by checking for
            -- virtual envs in the local directory and for Pipenev/Poetry configs
            --python = ".venv/bin/python"
            -- Returns if a given file path is a test file.
            -- NB: This function is called a lot so don't perform any heavy tasks within it.
            -- is_test_file = function(file_path)
            --   ...
            -- end,
            -- !!EXPERIMENTAL!! Enable shelling out to `pytest` to discover test
            -- instances for files containing a parametrize mark (default: false)
            pytest_discover_instances = true,
        }),
    },
})

neotest.setup_project(
    "~/code/orangeqs/scqt", {
        adapters = {
            require("neotest-python")({
                animated = false,
                dap = { justMyCode = false },
                -- args = {"--log-level", "WARN", "--slow"},
                args = { "--log-level", "WARN" },
                runner = "pytest",
            })
        },
    }
)

remap('n', 'ti', function() neotest.summary.toggle() end)
remap('n', 'to', function() neotest.output.open({ enter = true }) end)
remap('n', 'tp', function() neotest.output_panel.toggle() end)
remap('n', 'tt', function() neotest.run.run_last({ strategy = "integrated" }) end)
remap('n', '<c-d>t', function() neotest.run.run_last({ strategy = "dap" }) end)
remap('n', 'tc', function() neotest.run.run({ strategy = "integrated" }) end)
remap('n', '<c-d>c', function() neotest.run.run({ strategy = "dap" }) end)
remap('n', 'tf', function() neotest.run.run({ vim.fn.expand("%"), strategy = "integrated" }) end)
remap('n', '<c-d>f', function() neotest.run.run({ vim.fn.expand("%"), strategy = "dap" }) end)
