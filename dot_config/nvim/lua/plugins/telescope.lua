local telescope = require('telescope')

telescope.setup({
	extension = {
		coc = {
			theme = 'ivy',
			prefer_locations = true, -- always use Telescope locations to preview definitions/declarations/implementations etc
		},
	},
	defaults = {
		vimgrep_arguments = {
			"rg",
			"--color=never",
			"--no-heading",
			"--with-filename",
			"--line-number",
			"--column",
			"--smart-case",
		}
	}
})

telescope.load_extension('fzf')
telescope.load_extension('coc')
telescope.load_extension('repo')
telescope.load_extension('dap')
