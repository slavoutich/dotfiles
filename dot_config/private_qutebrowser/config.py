# Autogenerated config.py
#
# NOTE: config.py is intended for advanced users who are comfortable
# with manually migrating the config file on qutebrowser upgrades. If
# you prefer, you can also configure qutebrowser using the
# :set/:bind/:config-* commands without having to write a config.py
# file.
#
# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

import os

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    # pylint: disable=C0111
    from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
    from qutebrowser.config.config import ConfigContainer  # noqa: F401

    config: ConfigAPI = config  # noqa: F821  # pyright: ignore[reportUndefinedVariable]
    c: ConfigContainer = c  # noqa: F821  # pyright: ignore[reportUndefinedVariable]


# Change the argument to True to still load settings configured via autoconfig.yml
config.load_autoconfig(True)

# Which cookies to accept. With QtWebEngine, this setting also controls
# other features with tracking capabilities similar to those of cookies;
# including IndexedDB, DOM storage, filesystem API, service workers, and
# AppCache. Note that with QtWebKit, only `all` and `never` are
# supported as per-domain values. Setting `no-3rdparty` or `no-
# unknown-3rdparty` per-domain on QtWebKit will have the same effect as
# `all`. If this setting is used with URL patterns, the pattern gets
# applied to the origin/first party URL of the page making the request,
# not the request URL. With QtWebEngine 5.15.0+, paths will be stripped
# from URLs, so URL patterns using paths will not match. With
# QtWebEngine 5.15.2+, subdomains are additionally stripped as well, so
# you will typically need to set this setting for `example.com` when the
# cookie is set on `somesubdomain.example.com` for it to work properly.
# To debug issues with this setting, start qutebrowser with `--debug
# --logfilter network --debug-flag log-cookies` which will show all
# cookies being set.
# Type: String
# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only. This is known to break some sites, such as GMail.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
#   - never: Don't accept cookies at all.
c.content.cookies.accept = "no-unknown-3rdparty"

# Which cookies to accept. With QtWebEngine, this setting also controls
# other features with tracking capabilities similar to those of cookies;
# including IndexedDB, DOM storage, filesystem API, service workers, and
# AppCache. Note that with QtWebKit, only `all` and `never` are
# supported as per-domain values. Setting `no-3rdparty` or `no-
# unknown-3rdparty` per-domain on QtWebKit will have the same effect as
# `all`. If this setting is used with URL patterns, the pattern gets
# applied to the origin/first party URL of the page making the request,
# not the request URL. With QtWebEngine 5.15.0+, paths will be stripped
# from URLs, so URL patterns using paths will not match. With
# QtWebEngine 5.15.2+, subdomains are additionally stripped as well, so
# you will typically need to set this setting for `example.com` when the
# cookie is set on `somesubdomain.example.com` for it to work properly.
# To debug issues with this setting, start qutebrowser with `--debug
# --logfilter network --debug-flag log-cookies` which will show all
# cookies being set.
# Type: String
# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only. This is known to break some sites, such as GMail.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
#   - never: Don't accept cookies at all.
config.set("content.cookies.accept", "all", "chrome-devtools://*")

# Which cookies to accept. With QtWebEngine, this setting also controls
# other features with tracking capabilities similar to those of cookies;
# including IndexedDB, DOM storage, filesystem API, service workers, and
# AppCache. Note that with QtWebKit, only `all` and `never` are
# supported as per-domain values. Setting `no-3rdparty` or `no-
# unknown-3rdparty` per-domain on QtWebKit will have the same effect as
# `all`. If this setting is used with URL patterns, the pattern gets
# applied to the origin/first party URL of the page making the request,
# not the request URL. With QtWebEngine 5.15.0+, paths will be stripped
# from URLs, so URL patterns using paths will not match. With
# QtWebEngine 5.15.2+, subdomains are additionally stripped as well, so
# you will typically need to set this setting for `example.com` when the
# cookie is set on `somesubdomain.example.com` for it to work properly.
# To debug issues with this setting, start qutebrowser with `--debug
# --logfilter network --debug-flag log-cookies` which will show all
# cookies being set.
# Type: String
# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only. This is known to break some sites, such as GMail.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
#   - never: Don't accept cookies at all.
config.set("content.cookies.accept", "all", "devtools://*")

# Value to send in the `Accept-Language` header. Note that the value
# read from JavaScript is always the global value.
# Type: String
config.set("content.headers.accept_language", "", "https://matchmaker.krunker.io/*")

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value. With QtWebEngine
# between 5.12 and 5.14 (inclusive), changing the value exposed to
# JavaScript requires a restart.
# Type: FormatString
config.set(
    "content.headers.user_agent",
    "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}",
    "https://web.whatsapp.com/",
)

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value. With QtWebEngine
# between 5.12 and 5.14 (inclusive), changing the value exposed to
# JavaScript requires a restart.
# Type: FormatString
config.set(
    "content.headers.user_agent",
    "Mozilla/5.0 ({os_info}; rv:131.0) Gecko/20100101 Firefox/131.0",
    "https://accounts.google.com/*",
)

c.content.blocking.method = "both"

# List of URLs to ABP-style adblocking rulesets.  Only used when Brave's
# ABP-style adblocker is used (see `content.blocking.method`).  You can
# find an overview of available lists here:
# https://adblockplus.org/en/subscriptions - note that the special
# `subscribe.adblockplus.org` links aren't handled by qutebrowser, you
# will instead need to find the link to the raw `.txt` file (e.g. by
# extracting it from the `location` parameter of the subscribe URL and
# URL-decoding it).
# Type: List of Url
c.content.blocking.adblock.lists = [
    "https://easylist.to/easylist/easylist.txt",
    "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt",
    "https://easylist.to/easylist/easyprivacy.txt",
]


# Load images automatically in web pages.
# Type: Bool
config.set("content.images", True, "chrome-devtools://*")

# Load images automatically in web pages.
# Type: Bool
config.set("content.images", True, "devtools://*")

# Enable JavaScript.
# Type: Bool
config.set("content.javascript.enabled", True, "chrome-devtools://*")

# Enable JavaScript.
# Type: Bool
config.set("content.javascript.enabled", True, "devtools://*")

# Enable JavaScript.
# Type: Bool
config.set("content.javascript.enabled", True, "chrome://*/*")

# Enable JavaScript.
# Type: Bool
config.set("content.javascript.enabled", True, "qute://*/*")

# Allow locally loaded documents to access remote URLs.
# Type: Bool
config.set(
    "content.local_content_can_access_remote_urls",
    True,
    "file:///home/slavoutich/.local/share/qutebrowser/userscripts/*",
)

# Allow locally loaded documents to access other local URLs.
# Type: Bool
config.set(
    "content.local_content_can_access_file_urls",
    False,
    "file:///home/slavoutich/.local/share/qutebrowser/userscripts/*",
)

# Editor (and arguments) to use for the `edit-*` commands. The following
# placeholders are defined:  * `{file}`: Filename of the file to be
# edited. * `{line}`: Line in which the caret is found in the text. *
# `{column}`: Column in which the caret is found in the text. *
# `{line0}`: Same as `{line}`, but starting from index 0. * `{column0}`:
# Same as `{column}`, but starting from index 0.
# Type: ShellCommand
c.editor.command = ["neovide", "{file}", "--", "-c", "normal {line}G{column0}l"]

# List of widgets displayed in the statusbar.
# Type: List of StatusbarWidget
# Valid values:
#   - url: Current page URL.
#   - scroll: Percentage of the current page position like `10%`.
#   - scroll_raw: Raw percentage of the current page position like `10`.
#   - history: Display an arrow when possible to go back/forward in history.
#   - search_match: A match count when searching, e.g. `Match [2/10]`.
#   - tabs: Current active tab, e.g. `2`.
#   - keypress: Display pressed keys when composing a vi command.
#   - progress: Progress bar for the current page loading.
#   - text:foo: Display the static text after the colon, `foo` in the example.
#   - clock: Display current time. The format can be changed by adding a format string via `clock:...`. For supported format strings, see https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes[the Python datetime documentation].
c.statusbar.widgets = [
    "keypress",
    "search_match",
    "url",
    "scroll",
    "history",
    "progress",
]

# Scaling factor for favicons in the tab bar. The tab size is unchanged,
# so big favicons also require extra `tabs.padding`.
# Type: Float
c.tabs.favicons.scale = 2.0

# Padding (in pixels) around text for tabs.
# Type: Padding
c.tabs.padding = {"bottom": 9, "left": 5, "right": 5, "top": 9}

# Position of the tab bar.
# Type: Position
# Valid values:
#   - top
#   - bottom
#   - left
#   - right
c.tabs.position = "right"

# Duration (in milliseconds) to show the tab bar before hiding it when
# tabs.show is set to 'switching'.
# Type: Int
c.tabs.show_switching_delay = 800

# Format to use for the tab title. The following placeholders are
# defined:  * `{perc}`: Percentage as a string like `[10%]`. *
# `{perc_raw}`: Raw percentage, e.g. `10`. * `{current_title}`: Title of
# the current web page. * `{title_sep}`: The string `" - "` if a title
# is set, empty otherwise. * `{index}`: Index of this tab. *
# `{aligned_index}`: Index of this tab padded with spaces to have the
# same   width. * `{relative_index}`: Index of this tab relative to the
# current tab. * `{id}`: Internal tab ID of this tab. * `{scroll_pos}`:
# Page scroll position. * `{host}`: Host of the current web page. *
# `{backend}`: Either `webkit` or `webengine` * `{private}`: Indicates
# when private mode is enabled. * `{current_url}`: URL of the current
# web page. * `{protocol}`: Protocol (http/https/...) of the current web
# page. * `{audio}`: Indicator for audio/mute status.
# Type: FormatString
c.tabs.title.format = "{audio}: {current_title}"

# Format to use for the tab title for pinned tabs. The same placeholders
# like for `tabs.title.format` are defined.
# Type: FormatString
c.tabs.title.format_pinned = "{index}"

# Width (in pixels or as percentage of the window) of the tab bar if
# it's vertical.
# Type: PercOrInt
c.tabs.width = "8%"

# Padding (in pixels) for tab indicators.
# Type: Padding
c.tabs.indicator.padding = {"bottom": 2, "left": 0, "right": 4, "top": 2}

# Maximum stack size to remember for tab switches (-1 for no maximum).
# Type: Int
c.tabs.focus_stack_size = 10

# Show tooltips on tabs. Note this setting only affects windows opened
# after it has been set.
# Type: Bool
c.tabs.tooltips = True

# Text color of the completion widget. May be a single color to use for
# all columns or a list of three colors, one for each column.
# Type: List of QtColor, or QtColor
c.colors.completion.fg = "#d8e2ec"

# Background color of the completion widget for odd rows.
# Type: QssColor
c.colors.completion.odd.bg = "#1d252c"

# Background color of the completion widget for even rows.
# Type: QssColor
c.colors.completion.even.bg = "#171d23"

# Foreground color of completion widget category headers.
# Type: QtColor
c.colors.completion.category.fg = "#b7c5d3"

# Background color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.bg = "#171d23"

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.top = "#171d23"

# Bottom border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.bottom = "#171d23"

# Foreground color of the selected completion item.
# Type: QtColor
c.colors.completion.item.selected.fg = "#d8e2ec"

# Background color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.bg = "#28323a"

# Top border color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.border.top = "#28323a"

# Bottom border color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.border.bottom = "#28323a"

# Foreground color of the matched text in the selected completion item.
# Type: QtColor
c.colors.completion.item.selected.match.fg = "#9ece6a"

# Foreground color of the matched text in the completion.
# Type: QtColor
c.colors.completion.match.fg = "#9ece6a"

# Color of the scrollbar handle in the completion view.
# Type: QssColor
c.colors.completion.scrollbar.fg = "#d8e2ec"

# Color of the scrollbar in the completion view.
# Type: QssColor
c.colors.completion.scrollbar.bg = "#171d23"

# Background color of the context menu. If set to null, the Qt default
# is used.
# Type: QssColor
c.colors.contextmenu.menu.bg = "#171d23"

# Foreground color of the context menu. If set to null, the Qt default
# is used.
# Type: QssColor
c.colors.contextmenu.menu.fg = "#d8e2ec"

# Background color of the context menu's selected item. If set to null,
# the Qt default is used.
# Type: QssColor
c.colors.contextmenu.selected.bg = "#28323a"

# Foreground color of the context menu's selected item. If set to null,
# the Qt default is used.
# Type: QssColor
c.colors.contextmenu.selected.fg = "#d8e2ec"

# Background color of disabled items in the context menu. If set to
# null, the Qt default is used.
# Type: QssColor
c.colors.contextmenu.disabled.bg = "#1d252c"

# Foreground color of disabled items in the context menu. If set to
# null, the Qt default is used.
# Type: QssColor
c.colors.contextmenu.disabled.fg = "#b7c5d3"

# Background color for the download bar.
# Type: QssColor
c.colors.downloads.bar.bg = "#171d23"

# Color gradient start for download text.
# Type: QtColor
c.colors.downloads.start.fg = "#171d23"

# Color gradient start for download backgrounds.
# Type: QtColor
c.colors.downloads.start.bg = "#7aa2f7"

# Color gradient end for download text.
# Type: QtColor
c.colors.downloads.stop.fg = "#171d23"

# Color gradient stop for download backgrounds.
# Type: QtColor
c.colors.downloads.stop.bg = "#89ddff"

# Foreground color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.fg = "#f7768e"

# Font color for hints.
# Type: QssColor
c.colors.hints.fg = "#171d23"

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
# Type: QssColor
c.colors.hints.bg = "#b7c5d3"

# Font color for the matched part of hints.
# Type: QtColor
c.colors.hints.match.fg = "#d8e2ec"

# Text color for the keyhint widget.
# Type: QssColor
c.colors.keyhint.fg = "#d8e2ec"

# Highlight color for keys to complete the current keychain.
# Type: QssColor
c.colors.keyhint.suffix.fg = "#d8e2ec"

# Background color of the keyhint widget.
# Type: QssColor
c.colors.keyhint.bg = "#171d23"

# Foreground color of an error message.
# Type: QssColor
c.colors.messages.error.fg = "#171d23"

# Background color of an error message.
# Type: QssColor
c.colors.messages.error.bg = "#f7768e"

# Border color of an error message.
# Type: QssColor
c.colors.messages.error.border = "#f7768e"

# Foreground color of a warning message.
# Type: QssColor
c.colors.messages.warning.fg = "#171d23"

# Background color of a warning message.
# Type: QssColor
c.colors.messages.warning.bg = "#bb9af7"

# Border color of a warning message.
# Type: QssColor
c.colors.messages.warning.border = "#bb9af7"

# Foreground color of an info message.
# Type: QssColor
c.colors.messages.info.fg = "#d8e2ec"

# Background color of an info message.
# Type: QssColor
c.colors.messages.info.bg = "#171d23"

# Border color of an info message.
# Type: QssColor
c.colors.messages.info.border = "#171d23"

# Foreground color for prompts.
# Type: QssColor
c.colors.prompts.fg = "#d8e2ec"

# Border used around UI elements in prompts.
# Type: String
c.colors.prompts.border = "#171d23"

# Background color for prompts.
# Type: QssColor
c.colors.prompts.bg = "#171d23"

# Foreground color for the selected item in filename prompts.
# Type: QssColor
c.colors.prompts.selected.fg = "#d8e2ec"

# Background color for the selected item in filename prompts.
# Type: QssColor
c.colors.prompts.selected.bg = "#28323a"

# Foreground color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.fg = "#9ece6a"

# Background color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.bg = "#171d23"

# Foreground color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.fg = "#171d23"

# Background color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.bg = "#7aa2f7"

# Foreground color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.fg = "#171d23"

# Background color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.bg = "#89ddff"

# Foreground color of the statusbar in private browsing mode.
# Type: QssColor
c.colors.statusbar.private.fg = "#171d23"

# Background color of the statusbar in private browsing mode.
# Type: QssColor
c.colors.statusbar.private.bg = "#1d252c"

# Foreground color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.fg = "#d8e2ec"

# Background color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.bg = "#171d23"

# Foreground color of the statusbar in private browsing + command mode.
# Type: QssColor
c.colors.statusbar.command.private.fg = "#d8e2ec"

# Background color of the statusbar in private browsing + command mode.
# Type: QssColor
c.colors.statusbar.command.private.bg = "#171d23"

# Foreground color of the statusbar in caret mode.
# Type: QssColor
c.colors.statusbar.caret.fg = "#171d23"

# Background color of the statusbar in caret mode.
# Type: QssColor
c.colors.statusbar.caret.bg = "#bb9af7"

# Foreground color of the statusbar in caret mode with a selection.
# Type: QssColor
c.colors.statusbar.caret.selection.fg = "#171d23"

# Background color of the statusbar in caret mode with a selection.
# Type: QssColor
c.colors.statusbar.caret.selection.bg = "#7aa2f7"

# Background color of the progress bar.
# Type: QssColor
c.colors.statusbar.progress.bg = "#7aa2f7"

# Default foreground color of the URL in the statusbar.
# Type: QssColor
c.colors.statusbar.url.fg = "#d8e2ec"

# Foreground color of the URL in the statusbar on error.
# Type: QssColor
c.colors.statusbar.url.error.fg = "#f7768e"

# Foreground color of the URL in the statusbar for hovered links.
# Type: QssColor
c.colors.statusbar.url.hover.fg = "#d8e2ec"

# Foreground color of the URL in the statusbar on successful load
# (http).
# Type: QssColor
c.colors.statusbar.url.success.http.fg = "#89ddff"

# Foreground color of the URL in the statusbar on successful load
# (https).
# Type: QssColor
c.colors.statusbar.url.success.https.fg = "#9ece6a"

# Foreground color of the URL in the statusbar when there's a warning.
# Type: QssColor
c.colors.statusbar.url.warn.fg = "#bb9af7"

# Background color of the tab bar.
# Type: QssColor
c.colors.tabs.bar.bg = "#171d23"

# Color gradient start for the tab indicator.
# Type: QtColor
c.colors.tabs.indicator.start = "#7aa2f7"

# Color gradient end for the tab indicator.
# Type: QtColor
c.colors.tabs.indicator.stop = "#89ddff"

# Color for the tab indicator on errors.
# Type: QtColor
c.colors.tabs.indicator.error = "#f7768e"

# Foreground color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.fg = "#d8e2ec"

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = "#1d252c"

# Foreground color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.fg = "#d8e2ec"

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = "#171d23"

# Foreground color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.fg = "#d8e2ec"

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = "#28323a"

# Foreground color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.fg = "#d8e2ec"

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = "#28323a"

# Foreground color of pinned unselected odd tabs.
# Type: QtColor
c.colors.tabs.pinned.odd.fg = "#fbfbfd"

# Background color of pinned unselected odd tabs.
# Type: QtColor
c.colors.tabs.pinned.odd.bg = "#9ece6a"

# Foreground color of pinned unselected even tabs.
# Type: QtColor
c.colors.tabs.pinned.even.fg = "#fbfbfd"

# Background color of pinned unselected even tabs.
# Type: QtColor
c.colors.tabs.pinned.even.bg = "#89ddff"

# Foreground color of pinned selected odd tabs.
# Type: QtColor
c.colors.tabs.pinned.selected.odd.fg = "#d8e2ec"

# Background color of pinned selected odd tabs.
# Type: QtColor
c.colors.tabs.pinned.selected.odd.bg = "#28323a"

# Foreground color of pinned selected even tabs.
# Type: QtColor
c.colors.tabs.pinned.selected.even.fg = "#d8e2ec"

# Background color of pinned selected even tabs.
# Type: QtColor
c.colors.tabs.pinned.selected.even.bg = "#28323a"

# Value to use for `prefers-color-scheme:` for websites. The "light"
# value is only available with QtWebEngine 5.15.2+. On older versions,
# it is the same as "auto". The "auto" value is broken on QtWebEngine
# 5.15.2 due to a Qt bug. There, it will fall back to "light"
# unconditionally.
# Type: String
# Valid values:
#   - auto: Use the system-wide color scheme setting.
#   - light: Force a light theme.
#   - dark: Force a dark theme.
c.colors.webpage.preferred_color_scheme = "auto"

# Default font size to use. Whenever "default_size" is used in a font
# setting, it's replaced with the size listed here. Valid values are
# either a float value with a "pt" suffix, or an integer value with a
# "px" suffix.
# Type: String
c.fonts.default_size = "11pt"

cfg_dir = os.path.abspath(os.path.dirname(__file__))
for extra_config in (
    os.path.join(cfg_dir, "colors.py"),
    os.path.join(cfg_dir, "darkmode.py"),
):
    if os.path.exists(extra_config):
        with open(extra_config) as f:
            exec(f.read())

config.bind(
    "<Alt-Shift-u>",
    "spawn --userscript qute-keepassxc-wofi --key 0E83AB71A259C762AE7ABE0FDA9F6B51B19ADBF4",
    mode="insert",
)
config.bind(
    ",k",
    "spawn --userscript qute-keepassxc-wofi --key 0E83AB71A259C762AE7ABE0FDA9F6B51B19ADBF4",
    mode="normal",
)

config.bind("V", "spawn mpv {url}")
config.bind(";v", "hint links spawn mpv {hint-url}")
