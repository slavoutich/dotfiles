#!/usr/bin/python3
__doc__ = """Normalizes provided output name to the output port name.

If a name like "make model serial" is provided, its matching output is returned.
If output is provided (like HDMI-A-1), it is ensured to exist.
"""

import i3ipc
from typing import List


def normalize_output(name: str, outputs: List[i3ipc.replies.OutputReply]):
    matching_outputs = list(
        filter(
            lambda x: x.name == name or " ".join((x.make, x.model, x.serial)) == name,
            outputs,
        )
    )
    if len(matching_outputs) == 0:
        raise RuntimeError("No outputs match")
    elif len(matching_outputs) > 1:
        raise RuntimeError("Multiple outputs match")
    return matching_outputs[0].name


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "name",
        help='Either name of a display ouptut (like "HDMI-A-1") or a display name in '
        'a format "make model serial" (you can get it from '
        "`swaymsg -t get_outputs`)",
        type=str,
    )
    args = parser.parse_args()

    conn = i3ipc.Connection()
    outputs = conn.get_outputs()
    print(normalize_output(args.name, outputs))
